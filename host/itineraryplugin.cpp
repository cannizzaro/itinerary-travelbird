/*
    Copyright (C) 2019 by Kai Uwe Broulik <kde@privat.broulik.de>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include "itineraryplugin.h"

#include <QMetaEnum>
#include <QMimeDatabase>

#include "itineraryextractorjob.h"

ItineraryPlugin::ItineraryPlugin(QObject *parent)
    : AbstractBrowserPlugin(QStringLiteral("itinerary"), 1, parent)
    , m_supported(ItineraryExtractorJob::isSupported())
{

}

ItineraryPlugin::~ItineraryPlugin()
{

}

QJsonObject ItineraryPlugin::status() const
{
    return {
        {QStringLiteral("extractorFound"), m_supported}
    };
}

bool ItineraryPlugin::onLoad()
{
    // Check again on load, so reloading the plugin updates
    m_supported = ItineraryExtractorJob::isSupported();
    return m_supported;
}

bool ItineraryPlugin::onUnload()
{
    return true;
}

QJsonObject ItineraryPlugin::handleData(int serial, const QString &action, const QJsonObject &data)
{
    if (action == QLatin1String("extract")) {

        const QString format = data.value(QStringLiteral("format")).toString();

        QByteArray inputData;

        if (format == QLatin1String("raw")) {
            qDebug() << "extracting raw itinerary";
            const QString rawMessage = data.value(QStringLiteral("rawMessage")).toString();

            auto *job = new ItineraryExtractorJob(rawMessage.toLatin1());
            job->setInputType(ItineraryExtractorJob::InputType::Email);
            connect(job, &KJob::result, this, [this, job, serial] {
                if (job->error()) {
                    sendReply(serial, {
                        {QStringLiteral("success"), false},
                        {QStringLiteral("errorCode"), job->error()},
                        {QStringLiteral("errorMessage"), job->errorText()}
                    });
                    return;
                }

                sendReply(serial, {
                    {QStringLiteral("success"), true},
                    {QStringLiteral("data"), job->extractedData()}
                });
            });

            job->start();

            return {};

        } else if (format == QLatin1String("htmlParts")) {
            qDebug() << "extracting html parts itinerary";

            const QJsonArray parts = data.value(QStringLiteral("parts")).toArray();

            //int pendingJobs = 0;
            QJsonArray results;

            for (auto it = parts.begin(), end = parts.end(); it != end; ++it) {
                const QJsonObject part = it->toObject();

                const QString contentType = part.value(QStringLiteral("contentType")).toString();
                const QString body = part.value(QStringLiteral("body")).toString();

                if (body.isEmpty()) {
                    continue;
                }

                ItineraryExtractorJob::InputType type = ItineraryExtractorJob::InputType::Any;
                if (contentType == QLatin1String("text/html")) {
                    type = ItineraryExtractorJob::InputType::Html;
                }

                if (type == ItineraryExtractorJob::InputType::Any) {
                    continue;
                }

                auto *job = new ItineraryExtractorJob(body.toUtf8());
                job->setInputType(type);
                connect(job, &KJob::result, this, [this, job, serial] {
                    if (job->error()) {
                        sendReply(serial, {
                            {QStringLiteral("success"), false},
                            {QStringLiteral("errorCode"), job->error()},
                            {QStringLiteral("errorMessage"), job->errorText()}
                        });
                        return;
                    }

                    sendReply(serial, {
                        {QStringLiteral("success"), true},
                        {QStringLiteral("data"), job->extractedData()}
                    });
                });

                job->start();

                // FIXME multiple
                return {};
            }
        }

        return {
            {QStringLiteral("success"), false},
            {QStringLiteral("errorCode"), -1},
            {QStringLiteral("errorMessage"), QStringLiteral("No viable input data received")}
        };

    } else if (action == QLatin1String("run")) {

        // TODO open file in Itinerary

    }

    return {};
}
