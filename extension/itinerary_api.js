/*
    Copyright (C) 2020 Kai Uwe Broulik <kde@broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var { ExtensionCommon } = ChromeUtils.import("resource://gre/modules/ExtensionCommon.jsm");
var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");

class ItineraryApi {
    // _fireActionActivated
};

var itinerary = new ItineraryApi();

var itinerary_api = class extends ExtensionCommon.ExtensionAPI {

    getAPI(context) {
        const impl = new ItineraryApiImpl();

        // FIXME The code below is just a super crude attempt at getting the
        // horrific HTML we generated earlier *somehow* show up in the UI
        // Obviously we should get the structured data from Itinerary passed
        // in here and have the UI dynamically created. "Dynamcally" as in
        // "createElement", not botched together "innerHTML string". :)
        return {
            itinerary_api: {
                onActionActivated: new ExtensionCommon.EventManager({
                    context,
                    name: "itinerary_api.onActionActivated",
                    register(fire) {
                        // FIXME
                        itinerary.fireActionActivated = fire;

                        return function() {
                            itinerary.fireActionActivated = null;
                        }
                    }
                }).api(),

                async showHtml(html) {
                    const document = impl.document;
                    const container = impl.getContainer();

                    if (!html) {
                        container.style.display = "none";
                        container.innerHTML = "";
                        return;
                    }

                    container.innerHTML = html;
                    container.style.display = "block";

                    container.querySelectorAll("a.button").forEach((button) => {
                        button.addEventListener("click", (e) => {
                            e.preventDefault();

                            if (itinerary.fireActionActivated) {
                                // TODO we should probably pass an email id or data or something as context?
                                itinerary.fireActionActivated.async(button.dataset.itineraryAction);
                            }
                        });
                    });

                }
            }
        };
    }

};

class ItineraryApiImpl {

    static containerDivId() {
        return "kde-itinerary-container";
    }

    get document() {
        const recentWindow = Services.wm.getMostRecentWindow(null);//"mail:3pane");
        if (!recentWindow) {
            return null;
        }

        return recentWindow.document;
    }

    /**
     * Gets or creates the Itinerary container div for the current window
     */
    getContainer() {
        const document = this.document;

        // The message wrapper in mail:3pane
        const messagePaneWrapper = document.getElementById("messagepanewrapper");
        // The general message pane
        const messagePane = document.getElementById("messagepane");

        if (!messagePaneWrapper && !messagePane) {
            return null;
        }

        let itinerary = document.getElementById(ItineraryApiImpl.containerDivId());
        if (!itinerary) {
            // Create it now

            // Ugly... but the message pane only wants *sidebars*, no horizontal bars above it :(
            if (messagePaneWrapper) {
                messagePaneWrapper.orient = "vertical";
            }

            itinerary = document.createElementNS("http://www.w3.org/1999/xhtml", "div");
            // Allow using HTML inside
            itinerary.setAttributeNS("http://www.w3.org/2000/xmlns", "html", "http://www.w3.org/1999/xhtml");

            itinerary.setAttributeNS("http://www.w3.org/2000/xmlns", "xul", "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");

            itinerary.id = ItineraryApiImpl.containerDivId();

            itinerary.style.width = "100%";
            itinerary.style.display = "none";

            if (messagePaneWrapper) {
                messagePaneWrapper.insertBefore(itinerary, messagePaneWrapper.firstChild);
            } else if (messagePane) {
                // Standalone email viewer
                messagePane.parentNode.insertBefore(itinerary, messagePane);
            }
        }

        return itinerary;
    }
};
