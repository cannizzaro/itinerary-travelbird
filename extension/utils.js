/*
    Copyright (C) 2020 Kai Uwe Broulik <kde@broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Utils {
    static upperCaseFirst(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    // returns an Object which only contains values for keys in allowedKeys
    static filterObject(obj, allowedKeys) {
        let newObj = {}

        // I bet this can be done in a more efficient way...
        for (const key in obj) {
            if (obj.hasOwnProperty(key) && allowedKeys.includes(key)) {
                newObj[key] = obj[key];
            }
        }

        return newObj;
    }

    // TODO have a dedicated locale / itinerary formatter helper?
    static formatSchemaDate(schemaDate) {
        let d = null;
        let tz = "";
        if (typeof schemaDate === "object" && schemaDate["@type"] === "QDateTime") {
            d = new Date(schemaDate["@value"]); // TODO timezone
            tz = schemaDate["timezone"];
        } else if (typeof schemaDate === "string") {
            d = new Date(schemaDate);
        }

        if (!d || isNaN(d.getTime())) {
            console.warn("Failed to format", schemaDate);
            return "";
        }

        let opts = {
            weekday: "short",
            day: "numeric",
            month: "short",
            year: "numeric",
            // TODO don't if we just have a date?
            hour: "2-digit",
            minute: "2-digit",
            // no seconds
            timeZoneName: "short"
        };

        if (tz) {
            opts.timeZone = tz;
        }

        return d.toLocaleString([], opts);
    };
};

/**
 * This class represents a connection to a native host
 */
class NativeHost {
    /*
     * Creates a connection to the host with given id
     */
    constructor(id) {
        this._id = id;

        this._currentRequestSerial = 0;
        this._pendingRequestResolvers = [];

        this._plugins = {};
    }

    /**
     * Connect to the native host
     */
    connect() {
        return new Promise((resolve, reject) => {
            if (this._connected) {
                return reject("Already connected");
            }

            this._receivedMessage = false;

            this._port = browser.runtime.connectNative(this._id);

            this._port.onDisconnect.addListener((port) => {
                console.log("Disconnected", port.error);
                this._connected = false;
                if (!this._receivedMessage) {
                    return reject(port.error);
                }

                // TODO emit a disconnected signal
            });

            this._port.onMessage.addListener((message) => {
                //console.log("got port message", message);
                this._connected = true;
                if (!this._receivedMessage) {
                    this._receivedMessage = true;
                    resolve();
                }

                const isReply = message.hasOwnProperty("replyToSerial");
                const replyToSerial = message.replyToSerial;

                if (isReply) {
                    const resolver = this._pendingRequestResolvers[replyToSerial];
                    if (resolver) {
                        resolver(message.payload);
                    } else {
                        console.warn("There is no reply resolver for message with serial", replyToSerial);
                    }
                    return;
                }

                const subsystem = message.subsystem;
                const action = message.action;

                const plugin = this._plugins[subsystem];
                if (plugin) {
                    const handler = plugin["on" + Utils.upperCaseFirst(action)];
                    if (typeof handler === "function") {
                        handler.call(plugin, message.payload);
                    } else {
                        plugin.onMessage(action, message.payload);
                    }
                } else {
                    console.warn("There is no plugin for subsystem", subsystem);
                }
            });
        });
    }

    /**
     * Sends the message to the host
     */
    send(message) {
        this._port.postMessage(message);
    }

    /*
     * Sends the message to the host and returns a promise with a reply
     */
    sendWithReply(message) {
        return new Promise((resolve, reject) => {
            if (!this._connected) {
                return reject("Host not connected");
            }

            let wireMessage = message || {};
            wireMessage.serial = this._currentRequestSerial++;

            this._pendingRequestResolvers.push(resolve);

            this._port.postMessage(wireMessage);
        });
    }

    addPlugin(plugin) {
        this._plugins[plugin.subsystem] = plugin;

        // FIXME only when host tells us
        if (plugin.onLoad() === false) {
            // returning undefined (nothing) is assumed to be success
            console.warn(`Plugin {plugin.subsystem} refused to load`);
        }
    }

    // _id
    // _port

    // _connected
    // _receivedMessage

    // _currentRequestSerial
    // _pendingRequestResolvers
};

class AbstractPlugin {
    constructor(host, subsystem) {
        this._host = host;
        this._subsystem = subsystem;
    }

    get subsystem() {
        return this._subsystem;
    }

    send(action, payload) {
        this._host.send(this._message(action, payload));
    }

    sendWithReply(action, payload) {
        return this._host.sendWithReply(this._message(action, payload));
    }

    onMessage(action, payload) {
        console.warn("IMPLEMENT ME");
    }

    onLoad() {
        return true;
    }

    onUnload() {
        return true;
    }

    _message(action, payload) {
        let message = {
            subsystem: this._subsystem,
            action
        };

        if (payload) {
            message.payload = payload;
        }

        return message;
    }

    // _host
    // _subsystem
}
