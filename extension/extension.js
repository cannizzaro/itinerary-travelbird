/*
    Copyright (C) 2020 Kai Uwe Broulik <kde@broulik.de>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class ItineraryPlugin extends AbstractPlugin {
    constructor(host) {
        super(host, "itinerary");

        this._cache = {};

        browser.itinerary_api.onActionActivated.addListener((id) => {
            // TODO
            console.log("Itinerary action", id, "invoked");
        });
    }

    onLoad() {
        browser.messageDisplay.onMessageDisplayed.addListener(this._onMessageDisplayed.bind(this));
    }

    onUnload() {
        //browser.messageDisplay.onMessageDisplayed.removeListener(this._onMessageDisplayed);
    }

    onMessage(action, payload) {
        //console.log("Itinerary action", action, payload);
    }

    _onMessageDisplayed(tabId, message) {

        const cache = this._cache[message.id];
        if (cache) {
            this._showItinerary(cache);
            return;
        }

        // Clear everything before loading new stuff
        this._showItinerary([]);

        new Promise((resolve, reject) => {

            // This is new in Thunderbird 72 and gives us the raw MIME (i.e. including attachments)
            if (browser.messages.getRaw) {
                browser.messages.getRaw(message.id).then((rawMessage) => {
                    this.sendWithReply("extract", {
                        format: "raw",
                        rawMessage
                    }).then(resolve, reject);
                });

                return;
            }

            browser.messages.getFull(message.id).then((fullMessage) => {
                let parts = [];

                const fetchPartsRecursively = (part) => {
                    (part.parts || []).forEach(fetchPartsRecursively);

                    const { body, contentType, size } = part;

                    // Effectively only the text and HTML contents
                    if (!body) {
                        return;
                    }

                    if (size > 4 * 1024 * 1024) { // 4 MiB
                        return;
                    }

                    parts.push({
                        body,
                        contentType,
                        size
                    });
                };

                fetchPartsRecursively(fullMessage);

                if (parts.length === 0) { // shouldn't happen
                    return reject("NO_USABLE_PARTS");
                }

                this.sendWithReply("extract", {
                    format: "htmlParts",
                    parts
                }).then(resolve, reject);

            });

        }).then((result) => {
            return new Promise((resolve, reject) => {
                if (!result.success) {
                    return reject(result.errorMessage);
                }

                resolve(result);
            });
        }).then((result) => {
            console.log("got itinerary result", result);
            const data = result.data || [];

            this._cache[message.id] = data;

            this._showItinerary(data);
        }, (err) => {
            console.warn("Failed to itinerary", err);
        });

    }

    // FIXME The code below is quite horrific and in dire need of someone
    // with experience with an HTML templating engine of some sort :)
    _showItinerary(data) {

        const css = `
.container {
    margin: 10px;
}

/* Mimic KMessageWidget */
.frame {
    display: block;
    margin: auto;
    box-shadow: 0px 0px 5px rgba(196,196,196,0.8);
    border: 1px solid #3daee9;
    background: #d6edf9;
    border-radius: 2px;
    max-width: 50em;
}

section {
    display: block;
    padding: 0px 5px;
}
section.actions {
    padding: 5px;
}
section:not(:last-child) {
    padding-bottom: 5px;
    border-bottom: 1px solid #a0b1b9;
}

section > table {
    width: 100%;
    border-collapse: collapse;
}
section > table > tr > td {
    width: 33%;
}
tr.airports > td > span {
    font-size: 200%;
}
section.flight > table tr > td:nth-child(2) {
    text-align: center;
}

section.actions {
    text-align:right;
}

/* FIXME figure out how to use proper button*/
a.button {
    display: inline-block;
    color: #232627!important;
    background-color: #eff0f1;
    background-image: linear-gradient(180deg,#f2f2f3,#e8e9ea);
    box-shadow: 0.5px 0.5px 0.5px 0.5px rgba(35,38,39,.1);
    border-color: #bcbebf!important;
    border-radius: 3px;
    border: 1px solid;
    padding: 6px 12px;
}
`;

        const html = (sections) => `
<style>${css}</style>
<?xml-stylesheet href="findfile.css" type="text/css"?>

<div class="container">
<div class="frame">

${sections}

</div>
</div>
`;

        let sectionsHtml = "";
        let sectionsDesc = [];

        data.forEach((item) => {
            const type = item["@type"];

            const trip = item.reservationFor;

            switch (type) {
            case "EventReservation": {

                sectionsHtml += `<section class="event">${Utils.formatSchemaDate(trip.startDate)}<br/>${trip.name}</section>`;
                break;
            }

            case "BusReservation": {
                const section = (trip) => `
<section class="flight">
    <table>
        <tr class="">
            <td><span>${trip.departureBusStop.name}</span></td>
            <td><span></span></td><!-- TODO Bus icon-->
            <td><span>${trip.arrivalBusStop.name}</span></td>
        </tr>
        <tr class="data">
            <td><span>${Utils.formatSchemaDate(trip.departureTime || trip.departureDay)}</span></td>
            <td><span>${trip.busNumber}</span></td>
            <td><span>${Utils.formatSchemaDate(trip.arrivalTime)}</span></td>
        </tr>
    </table>
</section>
`;
                sectionsHtml += section(trip);

                break;
            }

            case "TrainReservation": {
                const section = (trip) => `
<section class="flight">
    <table>
        <tr class="">
            <td><span>${trip.departureStation.name}</span></td>
            <td><span></span></td><!-- TODO Train icon-->
            <td><span>${trip.arrivalStation.name}</span></td>
        </tr>
        <tr class="data">
            <td><span>${Utils.formatSchemaDate(trip.departureTime || trip.departureDay)}</span></td>
            <td><span>${trip.trainNumber}</span></td>
            <td><span>${Utils.formatSchemaDate(trip.arrivalTime)}</span></td>
        </tr>
    </table>
</section>
`;
                sectionsHtml += section(trip);

                break;
            }

            case "FlightReservation": {
                const section = (trip) => `
<section class="flight">
    <table>
        <tr class="airports">
            <td><span>${trip.departureAirport.iataCode}</span></td>
            <td><span>&#9992;</span></td><!-- Airplane icon-->
            <td><span>${trip.arrivalAirport.iataCode}</span></td>
        </tr>
        <tr class="data">
            <td><span>${Utils.formatSchemaDate(trip.departureTime || trip.departureDay)}</span></td>
            <td><span>${trip.airline.iataCode} ${trip.flightNumber}</span></td>
            <td><span>${Utils.formatSchemaDate(trip.arrivalTime)}</span></td>
        </tr>
    </table>
</section>
`;
                sectionsHtml += section(trip);

                break;
            }

            }
        });

        if (sectionsHtml) {
            const actionButtons = (data[0].potentialAction || []).map((action) => {
                const type = action["@type"];
                let label = type;
                switch (type) {
                    case "CheckInAction":
                        label = "Check-in"; // TODO i18n
                        break;
                    case "DownloadAction":
                        label = "Download";
                        break;
                    case "UpdateAction":
                        label = "Modify";
                        break;
                }
                return `<a class="button" href="#" data-itinerary-action="${type}">${label}</a>`;
            });

            sectionsHtml += `
<section class="actions">
    <!--<a class="button" href="#">Add to KDE Itinerary</a>-->
    ${actionButtons.join(" ")}
</section>`;

            sectionsHtml = html(sectionsHtml);
        }

        // Right now we just send HTML we constructed here to the "experimental API"
        // which will then add it verbatim. Not very nice :)
        browser.itinerary_api.showHtml(sectionsHtml);
    }

    // _cache
}

class DebugPlugin extends AbstractPlugin {
    constructor(port) {
        super(host, "debug");
    }

    onMessage(action, payload) {
        switch (action) {
        case "debug":
            console.log("From host:", payload.message);
            break;
        case "warning":
            console.warn("From host:", payload.message);
            break;
        default:
            console.log("Host debug:", payload);
        }
    }
}

var host = new NativeHost("org.kde.itinerary.travelbird");
host.connect().then(() => {

    // FIXME we need to send some form of lifesign to the host
    // (Tech is from plasma-browser-integration which gets settings and what not delivered initially)
    host.send({hello: "world"});

}, (err) => {
    console.warn("Failed to connect to native host", err);
});

const itinerary = new ItineraryPlugin(host);
host.addPlugin(itinerary);

const debugPlugin = new DebugPlugin(host);
host.addPlugin(debugPlugin);
