# Itinerary Travelbird

This project aims to provide travel assistant capabilities to Mozilla Thunderbird by using KDE Itinerary.

For more information about KDE Itinerary, see https://phabricator.kde.org/project/profile/280/

This project is based on Plasma Browser Integration. For building, debugging, and usage instructions see the Firefox guide: https://community.kde.org/Plasma/Browser_Integration
